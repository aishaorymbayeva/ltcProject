from django import forms

from accounts.models import Group


class RegistrationForm(forms.Form):
    username = forms.CharField(label='Username', max_length=30)

    password1 = forms.CharField(label='Password',
                                widget=forms.PasswordInput())
    password2 = forms.CharField(label='Password (Again)',
                                widget=forms.PasswordInput())


class LoginForm(forms.Form):
    username = forms.CharField(label='Username', max_length=30)

    password1 = forms.CharField(label='Password',
                                widget=forms.PasswordInput())


class RegisterForGroup(forms.Form):

    code = forms.CharField(label='Group Code')

    def clean(self):
        cleaned_data = super(RegisterForGroup, self).clean()
        group = Group.objects.all().filter(code__exact=cleaned_data['code']).first()
        if group is None:
            self.add_error('code','There is no such group')


