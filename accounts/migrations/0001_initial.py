# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=200, blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Owner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=200)),
                ('surname', models.CharField(max_length=200, blank=True, null=True)),
                ('email_address', models.EmailField(max_length=254, blank=True, null=True)),
                ('phone', models.IntegerField(max_length=200, blank=True, null=True)),
                ('parent_phone', models.IntegerField(max_length=200, blank=True, null=True)),
                ('description', models.CharField(max_length=200)),
                ('user', models.OneToOneField(null=True, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Teacher',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=200)),
                ('description', models.CharField(max_length=200)),
                ('groups', models.ManyToManyField(to='accounts.Group')),
                ('similar', models.ManyToManyField(blank=True, null=True, related_name='similar_rel_+', to='accounts.Teacher')),
                ('user', models.OneToOneField(null=True, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='owner',
            name='teachers',
            field=models.ManyToManyField(to='accounts.Teacher'),
        ),
        migrations.AddField(
            model_name='owner',
            name='user',
            field=models.OneToOneField(null=True, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='group',
            name='students',
            field=models.ManyToManyField(to='accounts.Student'),
        ),
    ]
