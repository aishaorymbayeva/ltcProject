from django.db import models
from django.contrib.auth.models import User, AbstractUser


class User(AbstractUser):
    class Type:
        STUDENT = 0
        TEACHER = 1
        OWNER = 2

    name = models.CharField(max_length=200, null=True, blank=True)
    surname = models.CharField(max_length=200, blank=True, null=True)
    type = models.IntegerField(default=Type.STUDENT)
    email_address = models.EmailField(null=True, blank=True)
    phone = models.IntegerField(null=True, blank=True)
    parent_phone = models.IntegerField(null=True, blank=True)
    description = models.CharField(max_length=200)
    teachers = models.ManyToManyField('accounts.User', blank=True)

    def __str__(self):
        return str(self.id)


class Group(models.Model):
    name = models.CharField(max_length=200, null=True, blank=True)
    teacher = models.ForeignKey('accounts.User', null=True, blank=True)
    code = models.CharField(max_length=50)
    members = models.ManyToManyField('accounts.User',related_name='members', blank=True)

    def __str__(self):
        return self.name
