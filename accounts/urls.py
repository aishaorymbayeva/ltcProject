from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'home/$', views.home, name='home'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^signin/$', views.signin, name='signin'),
    url(r'^home/account/logout/$', views.logout_page, name='logout'),
    url(r'^group/$', views.group_register, name='group'),
]
