from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponseRedirect
from django.shortcuts import render, HttpResponse, redirect

from accounts.admin import MyUserCreationForm
from accounts.forms import RegistrationForm, LoginForm, RegisterForGroup
from accounts.models import User, Group


def home(request):
    groups = Group.objects.all().filter(teacher=request.user)

    return render(request, 'accounts/home.html', {'groups': groups})





def group_register(request):
    if request.method == 'POST':
        form = RegisterForGroup(request.POST)

        if form.is_valid():
            code = form.cleaned_data.get('code')
            group = Group.objects.all().filter(code__exact=code).first()
            group.members.add(request.user)
            return redirect('home')
    else:
        form = RegisterForGroup()

    return render(request, 'accounts/signin.html', {'form': form})


def logout_page(request):
    logout(request)
    return HttpResponseRedirect('home')


def signup(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            User.objects.create_user(username=form.cleaned_data['username'], password=form.cleaned_data['password1'])
            user = authenticate(username=username, password=raw_password)
            login(request, user)
        return redirect('home')
    else:
        form = RegistrationForm()
        print('asjdkl')
    return render(request, 'accounts/signin.html', {'form': form})


def signin(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        print(request.META)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
        return redirect('home')
    else:
        form = LoginForm()
    return render(request, 'accounts/signin.html', {'form': form})
